var fso = new ActiveXObject('Scripting.FileSystemObject');
var cwd = fso.GetAbsolutePathName('.');
var pf = fso.getFolder('.').ParentFolder;
var af, fc;

// clean target fodler
if (fso.FolderExists(pf+'\\turning-af')) {
    af = fso.GetFolder(pf+'\\turning-af\\');
    af.Delete(true);
}
af = fso.CreateFolder('..\\turning-af');

// copy new files
fso.CopyFolder(cwd+'\\prd', af+'\\prd', true);
fso.CopyFolder(cwd+'\\image', af+'\\image', true);
fso.CopyFile(cwd+'\\html\\*.html', af+'\\', true);

function ren(folderName) {
    var f = fso.GetFolder(af+'\\prd\\'+folderName);
    var ext, name;
    fc = new Enumerator(f.files);
    for (; !fc.atEnd(); fc.moveNext()) {
        ext = fso.GetExtensionName( fc.item() );
        name = fc.item().name.slice(0,(-34 - ext.length) ) + '.'+ext;
        fc.item().Move(f+'\\'+name);
    };
    
}
ren('styles');
ren('scripts');

var shell = WScript.CreateObject('WScript.Shell');
var btn = shell.Popup('update appfog?', 5, 'Dialog(5s)', 0x4+0x20);

if (btn === 6 || btn === -1) {
    shell.Run('cmd /k cd '+ af +'& af update sens');
} else {
	WScript.Echo('Done!');
}
