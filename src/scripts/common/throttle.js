function throttle(method, context, dur) {
    clearTimeout(method.tId);
    method.tId= setTimeout(function(){
        method.call(context);
    }, dur || 1000);
}

module.exports = throttle;