$(document).ready(function() {
    var bdv = $('.board-view');
    var H = bdv.parent().outerWidth();
    var $p = $('.see-next .prev');
    var $n = $('.see-next .next');

    $('body').on('click', '.see-next .snext', function(event) {
        event.preventDefault();
        var bdi = bdv.find('.board-inner');
        var active = bdi.filter('.active');
        var action = $(this).data('action');
        var magic = {'next': -1, 'prev': 1};
        // var idx = active.index();
        var h, next;
        if ( bdv.is('.animated') ) {
            return;
        }

        h = Math.max(active[0].scrollWidth, active[0].clientWidth, H);
        if (action === 'prev') {
            bdv.prepend( bdi.last() );
            bdv.css('marginLeft', magic[action] * -h);
        }
        next = active[action]('.board-inner');
        
        active.add(next).find('img.lazy').trigger('view');
        bdv.addClass('animated');
        bdv.animate({
            marginLeft: parseInt(bdv.css('marginLeft'), 10) + magic[action] * h
        }, 400, function () {
            active.removeClass('active');
            next.addClass('active');
            bdv.css('marginLeft', 0);
            if ( action === 'next') {
                bdv.append( bdi.first() );
            }
            bdv.removeClass('animated');
        });
    });

    $(".contact-board img.lazy").lazyload({
        event: "view"
    });
    $(".contact-board img.lazy:first-child").trigger('view');
});