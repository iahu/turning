template = '''<link rel="stylesheet" href="http://www.turningbrand.com/prd/styles/contentflow.css">
<div id="contentFlow" class="ContentFlow">
    <div class="loadIndicator">
        <div class="indicator"></div>
    </div>
    <div class="flow">
    </div>
    <div class="globalCaption"></div>
    <div class="scrollbar">
        <div class="slider"></div>
        <div class="position"></div>
    </div>
</div>
<div class="closer">&times;</div>'''

if module?
    module.exports = template
