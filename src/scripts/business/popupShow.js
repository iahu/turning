var ImageFlow = require('../common/imageflow.js');

$(document).ready(function() {
    var $win = $(window);
    var bdCont = $('.board-content');
    var buCont = $('.business-cont');
    var bdW = bdCont.width();
    var bdH = bdCont.height();
    var vW = $win.width();
    var vH = $win.height();
    var pos = buCont.position();
    var wrapper = document.createElement('div');
    var imageflow = new ImageFlow();
    wrapper.className = 'popup-wrapper';
    wrapper.style.display = 'none';
    document.body.appendChild(wrapper);
    wrapper.innerHTML = '<div class="imageflow"></div><div class="closer">&times;</div>'

    $(wrapper).on('click', '.closer', function(event) {
        event.preventDefault();
        wrapper.style.display = 'none';
        // enable nicescroll
        $('html').niceScroll();
        $('html, body').removeClass('popupshow');
    });
    $win.on('keyup', function(event) {
        if (event.which === 27) {
            if (wrapper.style.display === 'block') {
                $('.closer', wrapper).click();
            }
        }
    });

    function popHandler(event) {
        event.preventDefault();
        if (typeof listData === 'undefined') {return false;}
        var cid = +$(this).data('cid')-1 || 0;
        var list = listData[cid];
        var id = 'imageflow-'+ (+new Date());
        var html='',
            item, cfEl;

        for (var i = 0; i < list.length; i++) {
            html += ['<div class="img-item">'
                    ,'<img src="' + list[i].src + '" class="content" alt="'+ list[i].title +'"/>'
                    ,'<div class="reflection">'
                        , '<img src="'+ list[i].src +'" class="flip" />'
                        , '<div class="overlay"></div>'
                    ,'</div>'
                ,'</div>'].join('');
        }
        $(wrapper).height( $(window).outerHeight() );
        wrapper.firstChild.innerHTML = html;
        wrapper.firstChild.id = id;
        imageflow.init({
            reflections: false,
            slider: false,
            percentLandscape: 200,
            onClick: function () {},
            ImageFlowID: id
        });
        wrapper.style.display = 'block';
        // disable nicescroll
        $('html, body').addClass('popupshow');
        $('html').getNiceScroll().remove();
        $('html').css('overflow', '');
    }
    bdCont.on('click', '#slider .imgLink', popHandler);
});