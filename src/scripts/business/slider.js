var mcImgSlider = require('../common/js-image-slider.js');
var sliderOptions= {
    sliderId: "slider",
    startSlide: 0,
    effect: "17,2,8,14",
    // effectRandom: true,
    pauseTime: 2600,
    transitionTime: 500,
    slices: 11,
    boxes: 7,
    hoverPause: 2,
    autoAdvance: true,
    captionOpacity: 1,
    captionEffect: "fade",
    thumbnailsWrapperId: "thumbs",
    m: false,
    license: "b2e98"
};
$(function () {
    var imageSlider=new mcImgSlider(sliderOptions);
    var el = imageSlider.getElement('slider');

    $(el).after('<span class="arrow arrow-l"></span><span class="arrow arrow-r"></span>');
    $(el).parent().on('click', '.arrow', function(event) {
        event.preventDefault();
        var isLeft = $(this).is('.arrow-l');
        isLeft ? imageSlider.previous() : imageSlider.next();
    });

    // imageSlider.thumbnailPreview(function (thumbIndex) {
    //     return "<img src='/image/business/thumb-" + (thumbIndex + 1) + ".jpg' style='width:100px;height:60px;' />";
    // });
});
