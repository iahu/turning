var html = $('html');
var cover = $('.cover');
var bd = $('.body');
var fgbox = $('.cov-fg-box');
var labox = $('.cov-la-box');
var labtn = $('.la-btn');
var yue = fgbox.find('.fg-yue img');

$(document).ready(function() {
    var loader = new createjs.LoadQueue(true);
    loader.on("fileload", handleFileLoad);
    loader.on("progress", function (e) {
        if (console && console.log) {
            console.log(e.progress);
        }
        if (e.progress == 1) {
            handleFileLoad();
        }
    });
    loader.loadManifest([
        '/image/bg.jpg',
        '/image/anim.gif',
        '/image/cov-la.gif',
        '/image/cov-fg-bg.gif'
    ]);
    loader.load();
    function handleFileLoad() {
        html.addClass('page-show').show(400);
    }
});

function flushYue() {
	yue[0].src += '?' + (+new Date());
    yue.css('visibility', 'visible');
}
function showLa() {
    setTimeout(function() {
    	labox.fadeIn('slow');
    }, 2500);
	html.addClass('cover-show');
}
setTimeout(function() {
	window.scrollTo(0, 0);
}, 0);
if ( html.is('.cssanimations') ) {
	fgbox.addClass('animated fadeIn');
	fgbox.on('animationend webkitAnimationEnd', function () {
        flushYue();
        showLa();
    });
    bd.on('transitionEnd webkitTransitionEnd', function() {
        cover.hide();
    });
} else {
	fgbox.animate({
		opacity: 1
	}, 3000, function () {
        flushYue();
        showLa();
    });
}

function animateToMain() {
    // debugger;
    if (html.is('.cssanimations')) {
        html.addClass('view-scroll');
    } else {
        $('.body').css('marginTop','100%');
        html.addClass('view-scroll');
        $('.body').animate({marginTop: 0});
    }
}
function mouseScrollHandler(event) {
	// event.preventDefault();
	if ( html.is('.cover-show') ) {
		event = event.originalEvent;
		var y = event.wheelDelta || event.detail;
		if (y <= -120) {
			animateToMain();
		}
	}
}
labtn.on('click', animateToMain);
$(window).on('keyup', function(event) {
    if ( event.which === 40 && html.is('.cover-show')) animateToMain();
});
$(window).on('mousewheel', mouseScrollHandler);
$(window).on('DOMMouseScroll', mouseScrollHandler);
