var fs = require('fs');
var basePath = './image/works/';
var dWorks = fs.readdirSync(basePath);

dWorks.forEach(function(path) {
	var p = basePath + path;
	var p2 = p + '/data.txt';
	var output = [];
	if ( fs.statSync(p).isDirectory() ) {
		if (fs.existsSync(p2)) {
			var data = fs.readFileSync(p2, {encoding: 'utf8'}).split(/\r\n/);
			var d = fs.readdirSync(p).filter(function(f) {
				return f !== 'data.txt';
			});

			d.forEach(function(f, i) {
				var p3 = p+'/'+f;

				if ( fs.statSync(p3).isFile() ) {
					return;
				}

				var fd = fs.readdirSync(p3);
				var a = [];
				var t = data[i].trim();

				fd.forEach(function(m, j) {
					var p4 = p3 + '/' + m;
					if ( fs.statSync(p4).isFile() && m.substr(-4) === '.jpg' ) {
						a.push({
							src: p3 + '/' + m,
							title: t + '-' + (1+j)
						});
					}
				});
				output.push(a);
			});
		}
	}

	fs.writeFile(p+'/listData.json', JSON.stringify(output), function (err) {
		if (err) {
			throw err;
		}
		console.log( p+'/listData.json'+' saved!' );
	});

});